mirvncserver
============

(Forked from https://github.com/ycheng/mir-vnc-server)

Currently we don't have VNC server for UBPorts. This is an attempt at creating one.

Much of the keyboard/mouse input code was copied from https://github.com/hanzelpeter/dispmanx_vnc/blob/master/main.c

To run it out of the box (after installing the click) run the `MirVNCServer` application from the application dock. The program will continuously show the loading screen whilst running. To connect, you will need to get your device's IP and then connect to it using a VNC client using the IP address and port 5900.

To run with a 432x768 screen size use the following command:
./mirvncserver -m /run/mir_socket --cap-interval 2 -s 432 768

Rotation code from:
https://github.com/LibVNC/libvncserver/blob/master/examples/rotate.c
