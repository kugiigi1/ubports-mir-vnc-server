/*
 * Copyright (C) 2021  abmyii
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mirvncserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QtNetwork>
#include <QScreen>

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("mirvncserver.abmyii");
    //~ QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::setStyle("Suru");

    QGuiApplication app(argc, argv);

    qDebug() << "Starting app from main.cpp";

    QList<QHostAddress> ipList = QNetworkInterface::allAddresses();
    QString ipAddress;
    qreal refreshRate;
    int nativeOrientation;

    QScreen* screen = QGuiApplication::primaryScreen();
    refreshRate = screen->refreshRate();
    nativeOrientation = screen->nativeOrientation();

    for(int nIter=0; nIter<ipList.count(); nIter++)

    {
      if(!ipList[nIter].isLoopback())
          if (ipList[nIter].protocol() == QAbstractSocket::IPv4Protocol )
        ipAddress = ipList[nIter].toString();

    }

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("ipAddress", ipAddress);
    engine.rootContext()->setContextProperty("refreshRate", refreshRate);
    engine.rootContext()->setContextProperty("screenNativeOrientation", nativeOrientation);
    engine.load(QUrl(QStringLiteral("qrc:///Main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
