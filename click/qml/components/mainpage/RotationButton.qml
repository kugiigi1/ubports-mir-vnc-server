import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK
import QtSensors 5.2

Button {
	id: rotationButton
    
    property bool auto: false
    property bool lockAuto: false
    property int rotation: 0
    readonly property int rotationAngle: rotation * 90
    property int nativeOrientation: 1
    readonly property int nativeAngle: switch (nativeOrientation) {
                    case 1:
                        0
                        break;
                    case 2:
                        90
                        break;
                    case 4:
                        180
                        break;
                    case 8:
                        270
                        break;
                    default:
                        0
                }
	
	focusPolicy: Qt.NoFocus

	flat: true
    
    onAutoChanged: bindRotation()
    onEnabledChanged: bindRotation()
    
    Component.onCompleted: bindRotation()
    
    onClicked: {
        if (rotation < 3) {
            rotation += 1
        } else {
            rotation = 0
        }
    }

    function bindRotation() {
        rotation = Qt.binding(function(){return auto && !lockAuto ? orientationSensor.rotationValue : rotation})
    }
    
    OrientationSensor {
        id: orientationSensor
        
        readonly property var rotationEquiv:  {1 /* OrientationReading.TopUp */: 0,
                                              3 /* OrientationReading.LeftUp */: 1,
                                              2 /* OrientationReading.TopDown */: 2,
                                              4 /* OrientationReading.RightUp */: 3}
                                              
        readonly property int rotationValue: reading ? rotationEquiv[reading.orientation] : 0


        active: rotationButton.auto && !rotationButton.lockAuto
    }
	
	UITK.Icon {
		id: icon

        name: "phone-smartphone-symbolic"
		implicitWidth: Suru.units.gu(4)
		implicitHeight: implicitWidth
		anchors.centerIn: parent
        color: Suru.highlightColor
        Suru.highlightType: rotationButton.auto ? Suru.PositiveHighlight : Suru.InformationHighlight
        transform: Rotation { 
                origin.x: icon.width / 2
                origin.y: icon.height / 2
                angle: rotationButton.nativeAngle + rotationButton.rotationAngle
            }
	}

    Label {
        id: label

        text: rotationButton.auto ? i18n.tr("Au") : i18n.tr("Aa")
        font.pixelSize: Suru.units.gu(1.5)
        color: rotationButton.auto ? Suru.foregroundColor : Suru.highlightColor
        Suru.highlightType: rotationButton.auto ? Suru.PositiveHighlight : Suru.InformationHighlight
        anchors.centerIn: parent

        transform: Rotation { 
                origin.x: label.width / 2
                origin.y: label.height / 2
                angle: rotationButton.rotationAngle
            }
    }
}
