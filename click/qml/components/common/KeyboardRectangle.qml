import QtQuick 2.9
import QtQuick.Controls.Suru 2.2

Item{
	id: keyboardRectangle

	height: keyboard.target.visible ? keyboard.target.keyboardRectangle.height / (Suru.units.gridUnit / 8) : 0
	anchors{
		left: parent.left
		right: parent.right
		bottom: parent.bottom
	}
}
