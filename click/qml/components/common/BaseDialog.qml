import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2

Dialog {
    id: addDialog
    
    property real maximumWidth: Suru.units.gu(80)
    property real preferredWidth: parent.width * 0.80
    
    width: Math.min(preferredWidth, maximumWidth)

    x: (parent.width - width) / 2
    parent: ApplicationWindow.overlay

    modal: true
    
    standardButtons: Dialog.Ok | Dialog.Cancel
    
    function openBottom(){
        y = Qt.binding(function(){return (parent.height - height - Suru.units.gu(5))})
        open()
    }
    
    function openNormal(){
        y = Qt.binding(function(){return ((parent.height - height) / 2)})
        open()
    }
}
