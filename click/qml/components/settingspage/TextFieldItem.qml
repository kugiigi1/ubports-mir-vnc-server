import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3

RowLayout {
	id: textFieldItem
	
	property string title
    property alias value: textField.text
    property alias inputMethodHints: textField.inputMethodHints
    property alias validator: textField.validator
    property alias placeholderText: textField.placeholderText
	
	spacing: Suru.units.gu(2)

	anchors{
		left: parent.left
		right: parent.right
	}

	Label {
		id: label

        Layout.fillWidth: true
		Layout.alignment: Qt.AlignVCenter
		text: textFieldItem.title + ":"
		color: Suru.foregroundColor
        wrapMode: Text.WordWrap
	}

    TextField {
        id: textField

        Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
        Layout.preferredWidth: Suru.units.gu(10)
        horizontalAlignment: TextInput.AlignHCenter
    }
}
