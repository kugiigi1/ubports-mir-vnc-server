import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Suru 2.2

RowLayout {
	id: comboBoxItem
	
	property string text
	property alias currentIndex: comboBox.currentIndex
	property alias textRole: comboBox.textRole
	property alias model: comboBox.model
	
	spacing: Suru.units.gu(2)

	anchors {
		left: parent.left
		right: parent.right
	}

	Label {
		id: label

        Layout.fillWidth: true
		Layout.alignment: Qt.AlignVCenter
		text: comboBoxItem.text + ":"
		color: Suru.foregroundColor
        wrapMode: Text.WordWrap
	}

	ComboBox {
		id: comboBox
							
		Layout.preferredWidth: Suru.units.gu(25)
		Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
	}
}
