import QtQuick 2.9
import Qt.labs.settings 1.0


Item {
    id: settingsComponent
    
    //User data
    property alias firstRun: settings.firstRun
    property alias autoRotation: settings.autoRotation
    property alias askSuspension: settings.askSuspension
    
    //VNC Settings
    property alias resolutionFraction: settings.resolutionFraction
    property alias customHeight: settings.customHeight
    property alias customWidth: settings.customWidth
    property alias refreshRateFraction: settings.refreshRateFraction
    
    //App Settings
    property alias startonStartup: settings.startonStartup
    property alias alwaysShowLog: settings.alwaysShowLog

    Settings {
        id: settings

        //User data
        property bool firstRun: true
        property bool autoRotation: true
        property bool askSuspension: true
    
        //VNC Settings
        property real resolutionFraction: 3
        property int customHeight: 640
        property int customWidth: 360
        property int refreshRateFraction: 1
        
        //App Settings
        property bool startonStartup: false
        property bool alwaysShowLog: false
    }
}
